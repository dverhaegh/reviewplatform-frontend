import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Page from "../views/Page.vue";
import Questions from "../views/Questions.vue";
import CreateQuestion from "../views/CreateQuestion.vue";
import Tip from "../components/Tip.vue";
import Welcome from "../components/Welcome.vue";
import Goodbye from "../components/Goodbye.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/page",
    name: "page",
    component: Page,
    children: [
      { path: "welcome", component: Welcome },
      { path: "goodbye", component: Goodbye },
      { path: "tip", component: Tip }
    ]
  },
  {
    path: "/questions",
    name: "questions",
    component: Questions
  },
  {
    path: "/CreateQuestion",
    name: "CreateQuestion",
    component: CreateQuestion,
    props: true,
  },

];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
