/* eslint-disable */
let getters = {
    questions: state => {
        return state.questions
      },
    getQuestionById: state => id => {
      return state.questions.find(question => question.id === id)
    },

    answers: state => {
      return state.answers
    },

    getAnswerById: state => id => {
    return state.answers.find(answer => answer.id === id)
  },

  categories: state => {
    return state.categories
  },
  getCategoryById: state => id => {
  return state.categories.find(category => category.id === id)
  },
  userdata: state => {
    return state.userdata
  },  
  getUserById: state => id => {
    return state.userdata.find(user => user.id === id)
    },
}

export default getters