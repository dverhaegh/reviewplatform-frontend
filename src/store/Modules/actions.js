/* eslint-disable */
import EventService from "@/services/EventService.js";

let actions = {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // USER ACTIONS ///////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fetchUserData({ commit }) {
        EventService.getUserData()
            .then(res => {
                commit('FETCH_USERS', res.data)
            }).catch(error => {
            console.log(error) // eslint-disable-line
        })
    },

    fetchUser({ commit, getters }, id) {
        var event = getters.getUserById(id)

        if(event){
            commit('FETCH_USER', event)
        }else{
            EventService.getUser(id)
                .then(res => {
                    commit('FETCH_USER', res.data)
                    console.log(res.data)// eslint-disable-line
                }).catch(error => {
                console.log(error) // eslint-disable-line
            })
        }        
    },

    createUser({ commit }, user) {
        EventService.postUserData(user)
            .then(res => {
                commit('CREATE_USER', res.data)
                console.log('added') // eslint-disable-line
            })
            .catch(error => {
                console.log(error) // eslint-disable-line
            })
    },

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // QUESTION ACTIONS ///////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fetchQuestions({ commit }) {
        EventService.getQuestions()
            .then(res => {
                commit('FETCH_QUESTIONS', res.data)
            }).catch(error => {
            console.log(error) // eslint-disable-line
        })
    },

    fetchQuestion({ commit, getters }, id) {
        var event = getters.getQuestionById(id)

        if(event){
            commit('FETCH_QUESTION', event)
        }else{
            EventService.getQuestion(id)
                .then(res => {
                    commit('FETCH_QUESTION', res.data)
                    console.log(res.data)// eslint-disable-line
                }).catch(error => {
                console.log(error) // eslint-disable-line
            })
        }        
    },

    createQuestion({ commit }, question) {
        EventService.postQuestion(question)
            .then(res => {
                commit('CREATE_QUESTION', res.data)
                console.log('added question') // eslint-disable-line
            })
            .catch(error => {
                console.log(error) // eslint-disable-line
            })
    },

    editQuestion({ commit }, qstn) {
        EventService.editQuestion(qstn)
            .then(res => {
                commit('EDIT_QUESTION', res.data)
            }).catch(error => {
            console.log(error) // eslint-disable-line
        })
    },

    deleteQuestion({ commit }, question) {
        EventService.deleteQuestion(question)
            .then(res => {
                if (res.data === 'ok')
                    commit('DELETE_QUESTION', question)
                    console.log(res.data) // eslint-disable-line
            }).catch(error => {
            console.log(error) 
        })
    },

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ANSWER ACTIONS /////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fetchAnswers({ commit }) {
        EventService.getAnswers()
            .then(res => {
                commit('FETCH_ANSWERS', res.data)
            }).catch(error => {
            console.log(error) // eslint-disable-line
        })
    },

    fetchAnswer({ commit, getters }, id) {
        var event = getters.getAnswerById(id)

        if(event){
            commit('FETCH_ANSWER', event)
        }else{
            EventService.getAnswer(id)
                .then(res => {
                    commit('FETCH_ANSWER', res.data)
                    console.log(res.data)// eslint-disable-line
                }).catch(error => {
                console.log(error) // eslint-disable-line
            })
        }        
    },

    createAnswer({ commit }, answer) {
        EventService.postAnswer(answer)
            .then(res => {
                commit('CREATE_ANSWER', res.data)
                console.log('added answer') // eslint-disable-line
            })
            .catch(error => {
                console.log(error) // eslint-disable-line
            })
    },

    editAnswer({ commit }, answr) {
        EventService.editAnswer(answr)
            .then(res => {
                commit('EDIT_ANSWER', res.data)
            }).catch(error => {
            console.log(error) // eslint-disable-line
        })
    },

    deleteAnswer({ commit }, answer) {
        EventService.deleteAnswer(answer)
            .then(res => {
                if (res.data === 'ok')
                    commit('DELETE_ANSWER', answer)
                    console.log(res.data)// eslint-disable-line
            }).catch(error => {
            console.log(error) // eslint-disable-line
        })
    },

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CATEGORY ACTIONS ///////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fetchCategories({ commit }) {
        EventService.getCategories()
            .then(res => {
                commit('FETCH_CATEGORIES', res.data)
            }).catch(error => {
            console.log(error) // eslint-disable-line
        })
    },

    fetchCategory({ commit, getters }, id) {
        var event = getters.getCategoryById(id)

        if(event){
            commit('FETCH_CATEGORY', event)
        }else{
            EventService.getCategory(id)
                .then(res => {
                    commit('FETCH_CATEGORY', res.data)
                    console.log(res.data)// eslint-disable-line
                }).catch(error => {
                console.log(error) // eslint-disable-line
            })
        }        
    },

    createCategory({ commit }, category) {
        EventService.postCategory(category)
            .then(res => {
                commit('CREATE_CATEGORY', res.data)
                console.log('added category') // eslint-disable-line
            })
            .catch(error => {
                console.log(error) // eslint-disable-line
            })
    },

    editCategory({ commit }, ctgry) {
        EventService.editCategory(ctgry)
            .then(res => {
                commit('EDIT_CATEGORY', res.data)
            }).catch(error => {
            console.log(error) // eslint-disable-line
        })
    },

    deleteCategory({ commit }, category) {
        EventService.deleteCategory(category)
            .then(res => {
                if (res.data === 'ok')
                    commit('DELETE_CATEGORY', category)
                    console.log(res.data)// eslint-disable-line
            }).catch(error => {
            console.log(error) // eslint-disable-line
        })
    },
   
}

export default actions