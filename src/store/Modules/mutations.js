/* eslint-disable */
let mutations = {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // USERDATA MUTATIONS /////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    FETCH_USERS(state, userdata) {
      return state.userdata = userdata
    },

    FETCH_USER(state, user) {
      return state.user = user
    },

    CREATE_USER(state, user) {
      state.userdata.push(user)
    },

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // QUESTION MUTATIONS /////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    FETCH_QUESTIONS(state, questions) {
      return state.questions = questions
    },

    FETCH_QUESTION(state, question) {
      return state.question = question
    },

    CREATE_QUESTION(state, question) 
    {
      state.questions.push(question)
    },

    EDIT_QUESTION(state, qstn) {
      let index = state.questions.findIndex(item => item.id ===qstn.id);
      if(index !== -1){
        state.questions.splice(index, 1, qstn);
      }
    },

    DELETE_QUESTION(state, question) {
      let index = state.questions.indexOf(question);
      state.questions.splice(index, 1);
    },

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ANSWER MUTATIONS ///////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    FETCH_ANSWERS(state, answers) {
      return state.answers = answers
    },

    FETCH_ANSWER(state, answer) {
      return state.answer = answer
    },

    CREATE_ANSWER(state, answer) {
      state.answers.push(answer)
    },

    EDIT_ANSWER(state, answr) {
      let index = state.answers.findIndex(item => item.id ===answr.id);
      if(index !== -1){
        state.answers.splice(index, 1, answr);
      }
    },

    DELETE_ANSWER(state, category) {
      let index = state.categories.indexOf(category);
      state.categories.splice(index, 1);
    },

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CATEGORY MUTATIONS ///////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    FETCH_CATEGORIES(state, categories) {
      return state.categories = categories
    },

    FETCH_CATEGORY(state, category) {
      return state.category = category
    },

    CREATE_CATEGORY(state, category) {
      state.categories.push(category)
    },

    EDIT_CATEGORY(state, ctgry) {
      let index = state.categories.findIndex(item => item.id ===ctgry.id);
      if(index !== -1){
        state.categories.splice(index, 1, ctgry);
      }
    },

    DELETE_CATEGORY(state, category) {
      let index = state.categories.indexOf(category);
      state.categories.splice(index, 1);
    },
    
}

export default mutations