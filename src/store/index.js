/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
import actions from './Modules/actions'
import mutations from './Modules/mutations'
import getters from './Modules/getters'
import state from "./Modules/state";

Vue.use(Vuex);

export default new Vuex.Store({
    state,
    mutations,
    getters,
    actions,
})

